import * as firebase from 'firebase';

const firebaseConfig = {
  apiKey: process.env.FIREBASE_API_KEY,
  authDomain: process.env.FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.FIREBASE_DATABASE_URL,
  projectId: process.env.FIREBASE_PROJECT_ID,
  storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.FIREBASE_APP_ID
};

firebase.initializeApp(firebaseConfig);

const database = firebase.database();

const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

export { firebase, googleAuthProvider, database as default };

//-----------------------------------------------//

// child removed
// database.ref('expenses').on('child_removed', (snapshot) => {
//   console.log(snapshot.key, snapshot.val());
// });

// // child changed
// database.ref('expenses').on('child_changed', (snapshot) => {
//   console.log(snapshot.key, snapshot.val());
// });

// // child added
// database.ref('expenses').on('child_added', (snapshot) => {
//   console.log(snapshot.key, snapshot.val());
// });

// database.ref('expenses').once('value').then((snapshot) => {
//   const expenses = [];

//   snapshot.forEach((childSnapshot) => {
//     expenses.push({
//       id: childSnapshot.key,
//       ...childSnapshot.val()
//     });
//   });

//   console.log(expenses);
// }).catch();

// database.ref('expenses').on('value', (snapshot) => {
//   const expenses = [];

//   snapshot.forEach((childSnapshot) => {
//     expenses.push({
//       id: childSnapshot.key,
//       ...childSnapshot.val()
//     });
//   });

//   console.log(expenses);
// })

// database.ref('expenses').push({
//   description: 'item desc',
//   note: '',
//   amount: 109500,
//   createdAt: 976564667
// });


// database.ref('notes/-LqwICeH7YiiF0HU1A45').remove();

// database.ref('notes').push({
//   title: 'Cooking Pot',
//   body: 'I have no idea...'
// });

// const firebaseNotes = {
//   notes: {
//     uniqueid1: {
//       title: 'First note!',
//       body: 'This is my note'
//     },
//     uniqueid2: {
//       title: 'Second note!',
//       body: 'This is another note.'
//     }
//   }
// };

// const notes = [{
//   id: '12',
//   title: 'First note!',
//   body: 'This is my note'
// }, {
//   id: '34',
//   title: 'Second note!',
//   body: 'This is another note.'
// }];

// database.ref().set(firebaseNotes);

/// READ DATA
// database.ref().once('value')
//   .then((snapshot) => {
//     const val = snapshot.val();
//     console.log(val);
//   }).catch((error) => {
//     console.log('Error fetching data', error);
//   });

// const onValueChange = database.ref().on('value', (snapshot) => {
//   console.log(snapshot.val());
// }, (error) => {
//   console.log('Error at fetching', error);
// });

// setTimeout(() => {
//   database.ref('age').set(30);
// }, 3500);

// setTimeout(() => {
//   database.ref().off(onValueChange);
// }, 7000);

// setTimeout(() => {
//   database.ref('age').set(35);
// }, 10500);

//---
// database.ref().on('value', (snapshot) => {
//   const val = snapshot.val();
//   console.log(`${val.name} is a ${val.job.title} at ${val.job.company}`);
// })


/// CREATE DATA
// database.ref().set({
//   name: 'Ricardo Sanchez',
//   age: 25,
//   stressLevel: 5,
//   job: {
//     title: 'Soft Dev',
//     company: 'My own'
//   },
//   location: {
//     city: 'Managua',
//     country: 'Nicaragua'
//   }
// }).then(() => {
//   console.log('Data saved');
// }).catch((error) => {
//   console.log('this failed: ', error);
// });

/// UPDATE DATA
// database.ref().update({
//   stressLevel: 9,
//   'job/company': 'Amazon',
//   'location/city': 'Seattle'
// });

/// REMOVE DATA
// database.ref('isSingle').set(null);

// database.ref('isSingle').remove()
//   .then(() => {
//     console.log('Data removed');
//   }).catch((error) => {
//     console.log('Data was not removed: ', error);
//   });