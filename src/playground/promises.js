const promise = new Promise((resolve, reject) => {
  setTimeout(() => {
    // resolve({
    //   name: 'Solin',
    //   age: 25
    // });
    reject('went wrong');
  }, 1500);
});

promise.then((data) => {
  console.log(data);
}).catch((error) => {
  console.log('error: ', error);
});