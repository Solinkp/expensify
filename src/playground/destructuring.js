//---------- OBJECT DESTRUCTURING

/* const person = {
  name: 'Solin',
  age: 25,
  def: 'other',
  location: {
    city: 'Managua',
    temp: 35
  }
};

const {name, age, def: defaultVal = 'Deft'} = person;

console.log(`${name} is ${age}. Default: ${defaultVal}`);

const {city, temp: temperature} = person.location;

console.log(`It's ${temperature} in ${city}.`) */

/* const book = {
  title: 'Ego is the enemy',
  author: 'Ryan Holiday',
  publisher: {
    name: 'Penguin'
  }
}

const {name: publisherName = 'Self-Published'} = book.publisher;

console.log(publisherName);
 */


 //---------- ARRAY DESTRUCTURING
/*
  const address = ['1324 St Juniper', 'Phily', 'Pensil', '19854'];

  //const [, , state] = address; =====> skipping elements, if not needed leave the comma, for last just remove it
  const [street, city, state = 'New York', zip] = address;

  console.log(`You are in ${city} ${state}.`); */

  const menu = ['coffe (hot)', '$2.00', '$2.50', '$3.00'];

  const [item, , medium] = menu;

  console.log(`A medium ${item} costs ${medium}`);